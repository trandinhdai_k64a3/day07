
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="./css/style.css" rel="stylesheet">
    <title>Danh sách tân sinh viên</title>
</head>
<body>

    <?php
        $list_student = array(
                'Nguyễn Văn A' => 'Khoa học máy tính',
                'Trần Thị B' => 'Khoa học máy tính',
                'Nguyễn Hoàng C' => 'Khoa học vật liệu',
                'Đinh Quang D' => 'Khoa học vật liệu',
        );
    ?>

    <div class="center">
        <div class="container" style="border:none">
            <form name='formSearch' action='list_tsv.php' method="POST" enctype="multipart/form-data">
                <div class="border_box">
                    <span class="label" style="background:#ffffff; border:none; color:black">Khoa</span>
                    <select class="input" style="background:#cad6f7" name='department'>
                        <?php
                        $department = array("" => "", "MAT" => "Khoa học máy tính", "KDL" => "Khoa học vật liệu");
                        foreach ($department as $key => $value) {
                            echo '<option value="' . $key . '">' . $value . '</option>';
                        }
                        ?>
                    </select>
                </div>
                <div class="border_box">
                    <span class="label" style="background:#ffffff; border:none; color:black">Từ khóa</span>
                     <input class="input" style="background:#cad6f7" type="text" name="key_word" > 
                </div>
                <div class="login-btn" >
                    <form action="">
                        <button class="button_submit btn-search" type="submit" name="search">Tìm kiếm</button>
                    </form>
                </div>
                <div class="notion">
                    <div style="margin-right: 283px;">
                        Số sinh viên tìm thấy: <!--<?php echo $count ?>-->
                    </div>
                    <form action="form_tsv.php">
                        <button class="button_submit btn-add" type="submit" name="add">Thêm</button>
                    </form>
                </div>
                <div class="table_tsv">
                    <table>
                        <tr>
                            <th>No</th>
                            <th>Tên sinh viên</th>
                            <th>Khoa</th>
                            <th>Action</th>
                        </tr>
                        <th>

                        <?php
                            $count = 0;
                            foreach ($list_student as $key => $value) {
                                $count++;
                                echo '<tr>';
                                echo '<td>' . $count . '</td>';
                                echo '<td>' . $key . '</td>';
                                echo '<td>' . $value . '</td>';
                                echo '<td><button class="delNupdate" type="submit" name="edit">Sửa</button>'
                                    .' '. '<button class="delNupdate" type="submit" name="delete">Xóa</button></td>';
                                echo '</tr>';
                            }
                        ?>
                        </th>
                    </table>
                </div>
            </form>
        </div>
        
    </div>
</body>
</html>